terraform {
  backend "http" {
  }
}

provider "aws" {
  region = "eu-west-2"
}

data "aws_vpc" "default" {
  default = true
}

data "aws_subnet_ids" "all" {
  vpc_id = data.aws_vpc.default.id
}

module "db" {
  source  = "terraform-aws-modules/rds-aurora/aws"
  version = "~> 2.0"

  name                            = "production-aurora-postgresql"
  engine                          = "aurora-postgresql"
  engine_version                  = "11.6"

  vpc_id                          = data.aws_vpc.default.id
  subnets                         = data.aws_subnet_ids.all.ids

  replica_count                   = 1
  allowed_cidr_blocks             = ["0.0.0.0/0"]

  # Change to something more production oriented like db.r4.large
  instance_type                   = "db.t3.medium"

  # Uncomment this to enable replica scale
  replica_scale_enabled           = true
  replica_scale_min               = 1
  replica_scale_max               = 1

  apply_immediately               = true
  publicly_accessible             = true
  username                        = "postgres"

  tags                            = {
    Environment = "dev"
    Terraform   = "true"
  }
}
